const link = document.querySelectorAll('#navItem');

link.forEach((item)=>{
    item.addEventListener('click',()=>
    {
        let el = document.getElementById(item.getAttribute('data-link'));
        el.scrollIntoView({behavior:'smooth',block:'center'});
    });
});

document.addEventListener('scroll',()=>{
    const navbar = document.querySelector('#navbar')

    const scrolled = window.pageYOffset

    if(scrolled>690 && scrolled<1560){
        navbar.classList.add('navbar-light')
        navbar.classList.remove('navbar-dark')
    }else if (scrolled>2350) {
        navbar.classList.add('navbar-light')
        navbar.classList.remove('navbar-dark')
    }
    else{
        navbar.classList.remove('navbar-light')
        navbar.classList.add('navbar-dark')
    }
})